#!/usr/bin/env groovy
import jenkins.model.Jenkins

def systemMessage = "Lorem ipsum dolor sit amet"

Jenkins jenkins = Jenkins.getInstance()
jenkins.setSystemMessage(systemMessage)
jenkins.save()
