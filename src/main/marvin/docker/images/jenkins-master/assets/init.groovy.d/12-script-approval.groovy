#!/usr/bin/env groovy
import jenkins.model.*;
import org.jenkinsci.plugins.scriptsecurity.scripts.*;
import org.jenkinsci.plugins.scriptsecurity.sandbox.whitelists.*;

class SignatureApproval {

    static ScriptApproval scriptApproval = ScriptApproval.get()
    static HashSet alreadyApproved = new HashSet<>(Arrays.asList(scriptApproval.getApprovedSignatures()))

    static void approve(String signature) {
        if (!alreadyApproved.contains(signature)) {
            scriptApproval.approveSignature(signature)
        }
    }

    static void save() {
        scriptApproval.save()
    }
}

// Whitelist
SignatureApproval.approve('staticMethod groovy.lang.GroovySystem getVersion')
SignatureApproval.approve('new java.io.File java.lang.String')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods write java.io.File java.lang.String')
SignatureApproval.approve('method java.io.File createNewFile')
SignatureApproval.approve('staticField groovy.io.FileType ANY')
SignatureApproval.approve('staticField groovy.io.FileType DIRECTORIES')
SignatureApproval.approve('staticField groovy.io.FileType FILES')
SignatureApproval.approve('method java.io.File getName')
SignatureApproval.approve('method java.io.File getAbsolutePath')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods eachFileRecurse java.io.File groovy.io.FileType')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods eachFileRecurse java.io.File groovy.lang.Closure')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods getText java.io.File')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods traverse java.io.File java.util.Map groovy.lang.Closure')

// Query REST APIs via GET from groovy
SignatureApproval.approve('method java.net.URL openConnection')
SignatureApproval.approve('method java.net.HttpURLConnection getResponseCode')
SignatureApproval.approve('method java.net.URLConnection getInputStream')
SignatureApproval.approve('staticMethod org.codehaus.groovy.runtime.DefaultGroovyMethods getText java.io.InputStream')

// DANGEROUS: scripts and pipelines can approve signatures on their own -> consider removing these lines when not running on localhost
SignatureApproval.approve('staticMethod org.jenkinsci.plugins.scriptsecurity.scripts.ScriptApproval get')
SignatureApproval.approve('method org.jenkinsci.plugins.scriptsecurity.scripts.ScriptApproval getApprovedSignatures')
SignatureApproval.approve('method org.jenkinsci.plugins.scriptsecurity.scripts.ScriptApproval approveSignature java.lang.String')
SignatureApproval.approve('method hudson.model.Saveable save')

SignatureApproval.save()
