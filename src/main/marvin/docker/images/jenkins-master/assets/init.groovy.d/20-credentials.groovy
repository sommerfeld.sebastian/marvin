#!/usr/bin/env groovy
/**
 * see: https://gist.github.com/ivan-pinatti/830ec918781060df03b12efd4a14096e
 */
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.CredentialsScope
import com.cloudbees.plugins.credentials.domains.Domain
import com.cloudbees.plugins.credentials.impl.*
import jenkins.model.Jenkins
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl
import hudson.util.Secret

/**
 * Set global credentials with type "username / password".
 *
 * @param desc
 * @param id
 * @param user
 * @param pass
 */
def setUsernamePasswordCredentials(final String desc, final String id, final String user, final String pass) {
    Jenkins jenkins = Jenkins.getInstance()
    def domain = Domain.global()
    def store = jenkins.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

    store.addCredentials(domain, new UsernamePasswordCredentialsImpl(CredentialsScope.GLOBAL, id, desc, user, pass))
    jenkins.save()
}

/**
 * Set global credentials with type "secret text".
 *
 * @param desc
 * @param id
 * @param secretText -> e.g. the token
 */
def setSecretCredentials(final String desc, final String id, final String secretText) {
    Jenkins jenkins = Jenkins.getInstance()
    def domain = Domain.global()
    def store = jenkins.getExtensionList("com.cloudbees.plugins.credentials.SystemCredentialsProvider")[0].getStore()

    store.addCredentials(domain, new StringCredentialsImpl(CredentialsScope.GLOBAL, id, desc, Secret.fromString(secretText)))
    jenkins.save()
}

/**
 * Read password from file for given ID.
 *
 * @param id
 * @return
 */
static final String readPasswd(final String id) {
    //String fileContents = new File('/var/jenkins_home/secrets/' + id + '.passwd').getText('UTF-8')
    String fileContents = new File('/var/jenkins_home/secrets/' + id + '.passwd').text
    return fileContents
}

setUsernamePasswordCredentials("Artifactory User for sommerfeld.jfrog.io", "artifactory-user", "deployment_user", readPasswd("artifactory-user"))
setUsernamePasswordCredentials("User for hub.docker.com", "dockerhub-user", "sommerfeldio", readPasswd("dockerhub-user"))
setUsernamePasswordCredentials("FTP User for www.sommerfeld.io", "website-ftp-prod", "f0146176", readPasswd("website-ftp-prod"))
setUsernamePasswordCredentials("FTP User for www.masterblender.de", "masterblender-ftp", "f0148525", readPasswd("masterblender-ftp"))
setUsernamePasswordCredentials("FTP User for www.numero-uno.de", "numero-uno-ftp", "f014869f", readPasswd("numero-uno-ftp"))

setSecretCredentials("Token to read API from gitlab.com", "gitlab-api-token", readPasswd("gitlab-api-token"))
setSecretCredentials("Token to use with sonarcloud.io", "sonarcloud-token", readPasswd("sonarcloud-token"))
