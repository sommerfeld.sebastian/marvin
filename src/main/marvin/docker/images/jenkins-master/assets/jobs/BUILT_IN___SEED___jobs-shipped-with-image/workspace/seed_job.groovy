#!/usr/bin/env groovy

/**
 * Create jobs which are shipped with this image (from iamges filesystem).
 * @return
 */
def createJobsFromFilesystem() {
    String rootFolder = "/var/jenkins_home/jobs/BUILT_IN___SEED___jobs-shipped-with-image/workspace/pipelines"
    dh = new File(rootFolder)
    dh.eachFile {

        String folderName = it.getName()

        pipelineJob(folderName) {
            definition {
                cps {
                    script(readFileFromWorkspace(rootFolder + "/" + folderName + "/Jenkinsfile"))
                    sandbox()
                }
            }
        }
    }
}

createJobsFromFilesystem()
