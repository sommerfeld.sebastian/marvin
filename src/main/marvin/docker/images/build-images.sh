#!/bin/bash
# @file build-docker-images.sh
# @brief Provisioning script for Vagrantbox ``marvin``.
#
# @description The scripts builds docker images from this repository (src/main/docker-images) in order to run the
# respective container. For started images see ``src/main/marvin/services/docker-compose.yml``.
#
# IMPORTANT: DON'T RUN THIS SCRIPT DIRECTLY - Script is invoked by Vagrant.
#
# ==== Arguments
#
# The script does not accept any parameters.

export IMAGE_PREFIX="marvin"
export IMAGE_TAG="latest"

# @description Build docker image to use inside vagrantbox.
#
# @arg $1 string image_name (= directory containing the Dockerfile) - mandatory
function buildImage() {
  if [ -z "$1" ]
  then
    echo -e "$LOG_ERROR Param missing: image_name"
    echo -e "$LOG_ERROR exit"
    exit 0
  fi

  echo "[INFO] Building $IMAGE_PREFIX/$1:$IMAGE_TAG"
  (
    cd "/vagrant/docker/images/$1" || exit

    echo "[INFO] Build image"
    docker build -t "$IMAGE_PREFIX/$1:$IMAGE_TAG" .
  )
  echo "[DONE] Finished building $IMAGE_PREFIX/$1:$IMAGE_TAG"
}

# Build images
buildImage "jenkins-master"
