#!/bin/bash
# @file run.sh
# @brief Start all services used for automation, testing and CICD tasks.
#
# @description This script starts all services used for automation, testing and CICD tasks as docker containers. When
# vagrantboxes are booted for the first time, all VMs get provisioned. Containers run in foreground.
#
# include::ROOT:partial$startpage/06-building-block-view-services.adoc[]
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO starting containers"
docker-compose up
