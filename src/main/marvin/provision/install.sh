#!/bin/bash
# @file install.sh
# @brief Provisioning script for Vagrantbox ``marvin``.
#
# @description The scripts installs software packages in Vagrantbox "marvin".
#
# IMPORTANT: DON'T RUN THIS SCRIPT DIRECTLY - Script is invoked by Vagrant.
#
# ===== Git
#
# There is no further configuration needed. The ``$HOME/.gitconfig`` file and ``$HOME/.ssh`` folder are already copied
# into the vagrantbox from the host machine (see Vagrantfile).
#
# CAUTION: When the ssh keys on the host machine change, the vagrantbox needs new provisioning. It is recommended to
# setup the machine from scratch again (run ``./stop.sh && ./clean.sh &&./start.sh``).
#
# ==== Arguments
#
# The script does not accept any parameters.

sudo apt-get -y update
#sudo apt-get -y upgrade
echo "[DONE] Run update + upgrade"

sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
echo "[DONE] Allow apt to use a repository over HTTPS"

sudo apt-get install -y ncdu
sudo apt-get install -y htop
sudo apt-get install -y git
echo "[DONE] Installed packages"
