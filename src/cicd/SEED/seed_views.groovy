#!/usr/bin/env groovy

/**
 * Create a List View in Jenkins for Jobs matching the given pattern.
 * @param pattern The pattern to select jobs - also serves as the name of the list view.
 * @return
 */
def createListeView(final String pattern) {
    listView(pattern) {
        jobs {
            regex "${pattern}___.*"
        }

        columns {
            status()
            weather()
            name()
            lastSuccess()
            lastFailure()
            lastDuration()
            nextLaunch()
            buildButton()
        }
    }
}

createListeView("sommerfeld-io")
createListeView("sommerfeld.sebastian")
