#!/bin/bash
# @file stop.sh
# @brief Shutdown all services.
#
# @description The scripts gracefully stops all services.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Shutting down Vagrant Boxes"
(
  cd src/main/marvin || exit
  vagrant halt
)

echo -e "$LOG_DONE All Vagrant Boxes are shut down"
