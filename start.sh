#!/bin/bash
# @file start.sh
# @brief Start all services used for automation, testing and CICD tasks.
#
# @description This script starts all services used for automation, testing and CICD tasks inside a vagrantbox. When
# vagrantboxes are booted for the first time, all VMs get provisioned.
#
# include::ROOT:partial$startpage/06-building-block-view-services.adoc[]
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Start Vagrant Boxes"
(
  cd src/main/marvin || exit
  vagrant validate
  vagrant up
)

echo -e "$LOG_DONE All services up and running"
