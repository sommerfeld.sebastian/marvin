#!/bin/bash
# @file clean.sh
# @brief Remove Vagrantbox and clean up filesystem.
#
# @description The scripts removes Vagrantbox and cleans up the filesystem. Run stop.sh first before cleaning up.
#
# ==== Arguments
#
# The script does not accept any parameters.

(
  cd src/main/marvin || exit

  echo -e "$LOG_INFO Remove all virtual machines"
  vagrant destroy -f

  echo -e "$LOG_INFO Clean up filesystem"
  rm -rf .vagrant
)

echo -e "$LOG_DONE All Vagrant Boxes are shut down and cleaned up"
